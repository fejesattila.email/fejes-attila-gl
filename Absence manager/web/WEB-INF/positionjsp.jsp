<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>POSITION</title>
        
        <jsp:include page="header.jsp"/>
    </head>
    <body>
       <h1>POSITION</h1>
       <br>
       <br>
       <div class="container">
        <form method="POST" action="/AbsenceEJB/PositionServlet"> 
           Position name: <input class="form-control" type="text" name="positionname" />
           <br>
           <br>
           Position is Dangerous?
           <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-outline-info">
              <input type="radio" name="dangerous" value="0" autocomplete="off"> NO
            </label>
            <label class="btn btn-outline-info">
              <input type="radio" name="dangerous" value="1" autocomplete="off"> YES, DANGEROUS
            </label></br>
               
                 <br>
           <br>
            <br>
              
               
          </div><br>
                    Role:<select class="form-control" name="positionrolename">
                           <c:forEach var="text" items="${list}">
                           <option>	${text} </option>

                           </c:forEach>
                     </select>        
           <br>
           <br>
            <br>
           <button class="btn btn-outline-danger">ADD POSITION</button>
        </form>
        </div>
    </body>
</html>

