<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CHILD</title>
        <jsp:include page="header.jsp"/>
        
    </head>
    <body>
        
        <form method="POST" action="/AbsenceEJB/ChildServlet"> 
           Birthday: <input class="form-control" type="date" name="birthday" value="2000-01-01" min="1918-01-01" max="2018-12-31" />
           <br>
           Handicapped?
           <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
              <input type="radio" name="handicapped" value="0" autocomplete="off"> NO
            </label>
            <label class="btn btn-secondary">
              <input type="radio" name="handicapped" value="1" autocomplete="off"> YES, HANDICAPPED
            </label>
            </div>
           <br>
           
           
           Parent_ID:<select class="form-control" name="parentId">
                       <c:forEach var="text" items="${list}">
                           <option>	${text} </option>

                           </c:forEach>
                  </select>
            
           
           <!--<br>
           Parent_ID: <input class="form-control" type="text" name="parentId" />-->
           <br>
           <button class="btn btn-danger">ADD CHILD</button> 
        </form>
        
    </body>
</html>
