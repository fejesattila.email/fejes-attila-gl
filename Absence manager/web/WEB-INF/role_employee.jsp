
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ALL</title>
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" >Távollét-nyilvántartó</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">Saját adatok <span class="sr-only">(current)</span></a>
                </li>
              </ul>
                <span class="navbar-text">
                <a class="nav-link" href="#">Beállítások</a>
              </span> 
                  <span class="navbar-text">
                      
                      <label>${username}</label>
              </span> 
              <span class="navbar-text">
                <a class="nav-link" href="/AbsenceEJB/LogoutServlet">Kilépés</a>
              </span>
            </div>
          </nav>
 
  <div class="table table-nonfluid">
        <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">-</th>
            <th scope="col">Napok száma</th>
            <th scope="col">-</th>
          </tr>
        </thead>
        <tbody>
          <tr class="table-light">
            <th scope="row">Éves szabadságkeret</th>
            <td>.</td>
            <td>.</td>
          </tr>
          <tr class="table-success">
            <th scope="row">Kivehető napok</th>
            <td>.</td>
            <td>.</td>
          </tr>
          <tr class="table-warning">
            <th scope="row">Kivett napok</th>
           <td>.</td>
            <td>.</td>
          </tr>
          <tr class="table-danger">
            <th scope="row">Táppénzen töltött napok</th>
            <td>.</td>
            <td>.</td>
          </tr>
          <tr class="table-info">
            <th scope="row">Százalékos arány</th>
            <td>.</td>
            <td>.</td>
          </tr>
        </tbody>
      </table> 
      </div>
        <form method="GET" action="/AbsenceEJB/AbsenceServlet"> 
        <button class="btn btn-outline-info">Távollét igénylése</button>
        </form>
    </body>
</html>
