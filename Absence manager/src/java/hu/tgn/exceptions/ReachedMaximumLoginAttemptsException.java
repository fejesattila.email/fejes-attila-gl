package hu.tgn.exceptions;

public class ReachedMaximumLoginAttemptsException extends Exception {

    public ReachedMaximumLoginAttemptsException() {
        super("Maximum login attempts has been reached!");
    }

}
