package hu.tgn.entitys;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fenyo
 */
@Entity
@Table(name = "EMPLOYEES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employees.findAll", query = "SELECT e FROM Employees e")
    , @NamedQuery(name = "Employees.findByEmployeeId", query = "SELECT e FROM Employees e WHERE e.employeeId = :employeeId")
    , @NamedQuery(name = "Employees.findByFirstname", query = "SELECT e FROM Employees e WHERE e.firstname = :firstname")
    , @NamedQuery(name = "Employees.findByLastname", query = "SELECT e FROM Employees e WHERE e.lastname = :lastname")
    , @NamedQuery(name = "Employees.findByBirthday", query = "SELECT e FROM Employees e WHERE e.birthday = :birthday")
    , @NamedQuery(name = "Employees.findByHandicapped", query = "SELECT e FROM Employees e WHERE e.handicapped = :handicapped")
    , @NamedQuery(name = "Employees.findByContractstyle", query = "SELECT e FROM Employees e WHERE e.contractstyle = :contractstyle")
    , @NamedQuery(name = "Employees.findByStartofwork", query = "SELECT e FROM Employees e WHERE e.startofwork = :startofwork")
    , @NamedQuery(name = "Employees.findByEndofwork", query = "SELECT e FROM Employees e WHERE e.endofwork = :endofwork")
    , @NamedQuery(name = "Employees.findByRemaininglastyearvacations", query = "SELECT e FROM Employees e WHERE e.remaininglastyearvacations = :remaininglastyearvacations")
    , @NamedQuery(name = "Employees.findByEmail", query = "SELECT e FROM Employees e WHERE e.email = :email")})
public class Employees implements Serializable {

    @OneToMany(mappedBy = "employeeId")
    private Collection<AbsenceRequest> absenceRequestCollection;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "s_employee", sequenceName = "s_employee", allocationSize = 1)
    @GeneratedValue(generator = "s_employee", strategy = GenerationType.SEQUENCE)

    @Basic(optional = false)
    @Column(name = "EMPLOYEE_ID")
    private int employeeId;
    @Basic(optional = false)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Basic(optional = false)
    @Column(name = "LASTNAME")
    private String lastname;
    @Basic(optional = false)
    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Basic(optional = false)
    @Column(name = "HANDICAPPED")
    private int handicapped;
    @Basic(optional = false)
    @Column(name = "CONTRACTSTYLE")
    private int contractstyle;
    @Basic(optional = false)
    @Column(name = "STARTOFWORK")
    @Temporal(TemporalType.DATE)
    private Date startofwork;
    @Column(name = "ENDOFWORK")
    @Temporal(TemporalType.DATE)
    private Date endofwork;
    @Column(name = "REMAININGLASTYEARVACATIONS")
    private int remaininglastyearvacations;
    @Column(name = "EMAIL")
    private String email;
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID")
    @ManyToOne
    private Departments departmentId;
    @JoinColumn(name = "POSITION_ID", referencedColumnName = "POSITION_ID")
    @ManyToOne(optional = false)
    private Positions positionId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeId")
    private Collection<Absences> absencesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departmentBossid")
    private Collection<Departments> departmentsCollection;
    @OneToMany(mappedBy = "parent2Id")
    private Collection<Children> childrenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentId")
    private Collection<Children> childrenCollection1;

    public Employees() {
    }

    public Employees(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employees(String firstname, String lastname, Date birthday, int handicapped, int contractstyle, Date startofwork, String email, Positions positionId) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.contractstyle = contractstyle;
        this.startofwork = startofwork;
        this.email = email;
        this.positionId = positionId;
    }

    public Employees(int employeeId, String firstname, String lastname, Date birthday, int handicapped, int contractstyle, Date startofwork) {
        this.employeeId = employeeId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.contractstyle = contractstyle;
        this.startofwork = startofwork;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getHandicapped() {
        return handicapped;
    }

    public void setHandicapped(int handicapped) {
        this.handicapped = handicapped;
    }

    public int getContractstyle() {
        return contractstyle;
    }

    public void setContractstyle(int contractstyle) {
        this.contractstyle = contractstyle;
    }

    public Date getStartofwork() {
        return startofwork;
    }

    public void setStartofwork(Date startofwork) {
        this.startofwork = startofwork;
    }

    public Date getEndofwork() {
        return endofwork;
    }

    public void setEndofwork(Date endofwork) {
        this.endofwork = endofwork;
    }

    public int getRemaininglastyearvacations() {
        return remaininglastyearvacations;
    }

    public void setRemaininglastyearvacations(int remaininglastyearvacations) {
        this.remaininglastyearvacations = remaininglastyearvacations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Departments getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Departments departmentId) {
        this.departmentId = departmentId;
    }

    public Positions getPositionId() {
        return positionId;
    }

    public void setPositionId(Positions positionId) {
        this.positionId = positionId;
    }

    @XmlTransient
    public Collection<Absences> getAbsencesCollection() {
        return absencesCollection;
    }

    public void setAbsencesCollection(Collection<Absences> absencesCollection) {
        this.absencesCollection = absencesCollection;
    }

    @XmlTransient
    public Collection<Departments> getDepartmentsCollection() {
        return departmentsCollection;
    }

    public void setDepartmentsCollection(Collection<Departments> departmentsCollection) {
        this.departmentsCollection = departmentsCollection;
    }

    @XmlTransient
    public Collection<Children> getChildrenCollection() {
        return childrenCollection;
    }

    public void setChildrenCollection(Collection<Children> childrenCollection) {
        this.childrenCollection = childrenCollection;
    }

    @XmlTransient
    public Collection<Children> getChildrenCollection1() {
        return childrenCollection1;
    }

    public void setChildrenCollection1(Collection<Children> childrenCollection1) {
        this.childrenCollection1 = childrenCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.absenceRequestCollection);
        hash = 17 * hash + this.employeeId;
        hash = 17 * hash + Objects.hashCode(this.firstname);
        hash = 17 * hash + Objects.hashCode(this.lastname);
        hash = 17 * hash + Objects.hashCode(this.birthday);
        hash = 17 * hash + this.handicapped;
        hash = 17 * hash + this.contractstyle;
        hash = 17 * hash + Objects.hashCode(this.startofwork);
        hash = 17 * hash + Objects.hashCode(this.endofwork);
        hash = 17 * hash + Objects.hashCode(this.remaininglastyearvacations);
        hash = 17 * hash + Objects.hashCode(this.email);
        hash = 17 * hash + Objects.hashCode(this.departmentId);
        hash = 17 * hash + Objects.hashCode(this.positionId);
        hash = 17 * hash + Objects.hashCode(this.absencesCollection);
        hash = 17 * hash + Objects.hashCode(this.departmentsCollection);
        hash = 17 * hash + Objects.hashCode(this.childrenCollection);
        hash = 17 * hash + Objects.hashCode(this.childrenCollection1);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employees other = (Employees) obj;
        if (this.employeeId != other.employeeId) {
            return false;
        }
        if (this.handicapped != other.handicapped) {
            return false;
        }
        if (this.contractstyle != other.contractstyle) {
            return false;
        }
        if (!Objects.equals(this.firstname, other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.absenceRequestCollection, other.absenceRequestCollection)) {
            return false;
        }
        if (!Objects.equals(this.birthday, other.birthday)) {
            return false;
        }
        if (!Objects.equals(this.startofwork, other.startofwork)) {
            return false;
        }
        if (!Objects.equals(this.endofwork, other.endofwork)) {
            return false;
        }
        if (!Objects.equals(this.remaininglastyearvacations, other.remaininglastyearvacations)) {
            return false;
        }
        if (!Objects.equals(this.departmentId, other.departmentId)) {
            return false;
        }
        if (!Objects.equals(this.positionId, other.positionId)) {
            return false;
        }
        if (!Objects.equals(this.absencesCollection, other.absencesCollection)) {
            return false;
        }
        if (!Objects.equals(this.departmentsCollection, other.departmentsCollection)) {
            return false;
        }
        if (!Objects.equals(this.childrenCollection, other.childrenCollection)) {
            return false;
        }
        if (!Objects.equals(this.childrenCollection1, other.childrenCollection1)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Employees{" + "employeeId=" + employeeId + ", firstname=" + firstname + ", lastname=" + lastname + ", birthday=" + birthday + ", handicapped=" + handicapped + ", contractstyle=" + contractstyle + ", startofwork=" + startofwork + ", endofwork=" + endofwork + ", remaininglastyearvacations=" + remaininglastyearvacations + ", email=" + email + ", departmentId=" + departmentId + ", positionId=" + positionId + '}';
    }

    @XmlTransient
    public Collection<AbsenceRequest> getAbsenceRequestCollection() {
        return absenceRequestCollection;
    }

    public void setAbsenceRequestCollection(Collection<AbsenceRequest> absenceRequestCollection) {
        this.absenceRequestCollection = absenceRequestCollection;
    }

}
