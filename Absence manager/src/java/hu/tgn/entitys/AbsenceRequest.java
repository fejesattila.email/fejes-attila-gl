package hu.tgn.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ABSENCE_REQUEST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AbsenceRequest.findAll", query = "SELECT a FROM AbsenceRequest a")
    , @NamedQuery(name = "AbsenceRequest.findById", query = "SELECT a FROM AbsenceRequest a WHERE a.id = :id")
    , @NamedQuery(name = "AbsenceRequest.findByStartDate", query = "SELECT a FROM AbsenceRequest a WHERE a.startDate = :startDate")
    , @NamedQuery(name = "AbsenceRequest.findByEndDate", query = "SELECT a FROM AbsenceRequest a WHERE a.endDate = :endDate")
    , @NamedQuery(name = "AbsenceRequest.findByType", query = "SELECT a FROM AbsenceRequest a WHERE a.type = :type")})
public class AbsenceRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "s_absencerequest", sequenceName = "s_absencerequest", allocationSize = 1)
    @GeneratedValue(generator = "s_absencerequest", strategy = GenerationType.SEQUENCE)

    @Basic(optional = false)
    @Column(name = "ID")
    private int id;

    @Column(name = "START_DATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "END_DATE")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "REQUESTTYPE")
    private String requestType;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "absenceRequest")
    private Absences absences;

    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "EMPLOYEE_ID")
    @ManyToOne
    private Employees employeeId;

    public AbsenceRequest() {
    }

    public AbsenceRequest(int id, Date startDate, Date endDate, String type, String status, String requestType, Absences absences, Employees employeeId) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.status = status;
        this.requestType = requestType;
        this.absences = absences;
        this.employeeId = employeeId;
    }

    public AbsenceRequest(int id) {
        this.id = id;
    }

    public AbsenceRequest(Date startDate, Date endDate, String type, String status, String requestType, Employees employeeId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.status = status;
        this.requestType = requestType;
        this.employeeId = employeeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Absences getAbsences() {
        return absences;
    }

    public void setAbsences(Absences absences) {
        this.absences = absences;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Employees getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employees employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.startDate);
        hash = 29 * hash + Objects.hashCode(this.endDate);
        hash = 29 * hash + Objects.hashCode(this.type);
        hash = 29 * hash + Objects.hashCode(this.absences);
        hash = 29 * hash + Objects.hashCode(this.employeeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbsenceRequest other = (AbsenceRequest) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        if (!Objects.equals(this.absences, other.absences)) {
            return false;
        }
        if (!Objects.equals(this.employeeId, other.employeeId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbsenceRequest{" + "id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", type=" + type + ", status=" + status + ", requestType=" + requestType + ", absences=" + absences + ", employeeId=" + employeeId + '}';
    }

}
