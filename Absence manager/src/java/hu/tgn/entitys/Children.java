package hu.tgn.entitys;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CHILDREN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Children.findAll", query = "SELECT c FROM Children c")
    , @NamedQuery(name = "Children.findByChildId", query = "SELECT c FROM Children c WHERE c.childId = :childId")
    , @NamedQuery(name = "Children.findByChildBirthday", query = "SELECT c FROM Children c WHERE c.childBirthday = :childBirthday")
    , @NamedQuery(name = "Children.findByHandicapped", query = "SELECT c FROM Children c WHERE c.handicapped = :handicapped")})
public class Children implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "s_child", sequenceName = "s_child", allocationSize = 1)
    @GeneratedValue(generator = "s_child", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "CHILD_ID")
    private int childId;
    @Basic(optional = false)
    @Column(name = "CHILD_BIRTHDAY")
    @Temporal(TemporalType.DATE)
    private Date childBirthday;
    @Basic(optional = false)
    @Column(name = "HANDICAPPED")
    private int handicapped;
    @JoinColumn(name = "PARENT2_ID", referencedColumnName = "EMPLOYEE_ID")
    @ManyToOne
    private Employees parent2Id;
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "EMPLOYEE_ID")
    @ManyToOne(optional = false)
    private Employees parentId;

    public Children() {
    }

    public Children(Date childBirthday, int handicapped, Employees parentId) {
        this.childBirthday = childBirthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
    }

    public Children(int childId, Date childBirthday, int handicapped, Employees parentId) {
        this.childId = childId;
        this.childBirthday = childBirthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
    }

    public Children(int childId, Date childBirthday, int handicapped, Employees parent2Id, Employees parentId) {
        this.childId = childId;
        this.childBirthday = childBirthday;
        this.handicapped = handicapped;
        this.parent2Id = parent2Id;
        this.parentId = parentId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public Date getChildBirthday() {
        return childBirthday;
    }

    public void setChildBirthday(Date childBirthday) {
        this.childBirthday = childBirthday;
    }

    public int getHandicapped() {
        return handicapped;
    }

    public void setHandicapped(int handicapped) {
        this.handicapped = handicapped;
    }

    public Employees getParent2Id() {
        return parent2Id;
    }

    public void setParent2Id(Employees parent2Id) {
        this.parent2Id = parent2Id;
    }

    public Employees getParentId() {
        return parentId;
    }

    public void setParentId(Employees parentId) {
        this.parentId = parentId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.childId;
        hash = 29 * hash + Objects.hashCode(this.childBirthday);
        hash = 29 * hash + this.handicapped;
        hash = 29 * hash + Objects.hashCode(this.parentId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Children other = (Children) obj;
        if (this.childId != other.childId) {
            return false;
        }
        if (this.handicapped != other.handicapped) {
            return false;
        }
        if (!Objects.equals(this.childBirthday, other.childBirthday)) {
            return false;
        }
        if (!Objects.equals(this.parentId, other.parentId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Children{" + "childId=" + childId + ", childBirthday=" + childBirthday + ", handicapped=" + handicapped + ", parent2Id=" + parent2Id + ", parentId=" + parentId + '}';
    }

}
