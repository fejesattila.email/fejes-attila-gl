package hu.tgn.mapper;

import hu.tgn.dtos.ChildrenDTO;
import hu.tgn.entitys.Children;
import hu.tgn.entitys.Employees;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class ChildMapper implements MapperService<Children, ChildrenDTO> {

    @Override
    public Children DTO2Entity(ChildrenDTO cdto) {
        Children child = new Children(
                cdto.getBirthday(),
                cdto.getHandicapped(),
                new Employees(cdto.getParentId()));
        return child;
    }

    @Override
    public ChildrenDTO Entity2DTO(Children entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
