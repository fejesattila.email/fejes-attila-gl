package hu.tgn.mapper;

import hu.tgn.dtos.AbsencesDTO;
import hu.tgn.entitys.AbsenceRequest;
import hu.tgn.entitys.Absences;
import hu.tgn.entitys.Employees;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class AbsenceMapper implements MapperService<Absences, AbsencesDTO> {

    @Override
    public Absences DTO2Entity(AbsencesDTO adto) {
        return new Absences(adto.getAbsenceDay(), adto.getAbsenceStatus(), adto.getAbsenceType(), new Employees(adto.getEmployeeId()), new AbsenceRequest(adto.getAbsenceRequestId()));
    }

    @Override
    public AbsencesDTO Entity2DTO(Absences abs) {
        return new AbsencesDTO(abs.getAbsenceId(), abs.getAbsenceDay(), abs.getAbsenceStatus(), abs.getAbsenceType(), abs.getAbsenceRequest().getId(), abs.getEmployeeId().getEmployeeId());
    }
}
