package hu.tgn.mapper;

import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.entitys.Employees;
import hu.tgn.entitys.Positions;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;

@Singleton
@LocalBean
public class EmployeeMapper implements MapperService<Employees, EmployeesDTO> {

    @Override
    public Employees DTO2Entity(EmployeesDTO dto) {
        return new Employees(dto.getFirstname(),
                dto.getLastname(),
                dto.getBirthday(),
                dto.getHandicapped(),
                dto.getContractstyle(),
                dto.getStartofwork(),
                dto.getEmail(),
                new Positions(dto.getPositionID()));
    }

    @Override
    public EmployeesDTO Entity2DTO(Employees entity) {
        EmployeesDTO edto = (new EmployeesDTO(
                entity.getEmployeeId(),
                entity.getFirstname(),
                entity.getLastname(),
                entity.getBirthday(),
                entity.getHandicapped(),
                entity.getContractstyle(),
                entity.getStartofwork(),
                entity.getEndofwork(),
                entity.getEmail(),
                entity.getPositionId().getPositionId()));
        return edto;

    }

}
