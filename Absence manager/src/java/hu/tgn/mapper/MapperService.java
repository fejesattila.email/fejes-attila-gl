package hu.tgn.mapper;

public interface MapperService<ENTITY, DTO> {

    public ENTITY DTO2Entity(DTO dto);

    public DTO Entity2DTO(ENTITY entity);

}
