package hu.tgn.rest;

import hu.tgn.beans.AbsenceBean;
import hu.tgn.dtos.EmployeesDTO;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("link")
@RequestScoped
public class Resource {

    @EJB
    AbsenceBean absenceBean;

    public Resource() {
    }

    @GET
    @Path("/dailyabsenceriport")
    @Produces(MediaType.TEXT_HTML)
    public String getUsers() {
        return "<html>"
                + "<head>"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                + "<title>RIPORT</title>"
                + header()
                + "</head>"
                + "<body>"
                + "<br>"
                + "<H1 align=\"center\" class=\"alert-heading\">ALL EMPLOYEES OUT OF OFFICE</H1>"
                + "<br>"
                + "<br>"
                + "<table class=\"table table-hover\">"
                + "<tbody>"
                + "<tr class=\"table-active\"><th scope=\"row\">Number</th><td>Name</td></tr>"
                + outOfOfficeList()
                + "</tbody>"
                + "</table> "
                + "</body>"
                + "</html>";
    }

    private String header() {
        return "<link rel=\"stylesheet\" href=\"https://bootswatch.com/4/superhero/bootstrap.min.css\" />\n"
                + "<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n"
                + "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>\n"
                + "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js\" integrity=\"sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em\" crossorigin=\"anonymous\"></script>\n";
    }

    private String outOfOfficeList() {
        List<EmployeesDTO> list = absenceBean.allEmployeesOutOfOffice(new Date());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append("<tr class=\"table-active\"><th scope=\"row\"> " + (i + 1) + "</th><td>" + list.get(i).getLastname() + " " + list.get(i).getFirstname() + "</td></tr>");
        }
        return sb.toString();
    }
}
