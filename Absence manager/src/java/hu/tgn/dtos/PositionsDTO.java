package hu.tgn.dtos;

import java.io.Serializable;

public class PositionsDTO implements Serializable {

    private int positionId;
    private String positionName;
    private int dangerous;
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public PositionsDTO() {
    }

    public PositionsDTO(int positionId) {
        this.positionId = positionId;
    }

    public PositionsDTO(String positionName, int dangerous) {
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public PositionsDTO(int positionId, String positionName, int dangerous) {
        this.positionId = positionId;
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public PositionsDTO(String positionName, int dangerous, String role) {
        this.positionName = positionName;
        this.dangerous = dangerous;
        this.role = role;
    }

    public PositionsDTO(int positionId, String positionName, int dangerous, String role) {
        this.positionId = positionId;
        this.positionName = positionName;
        this.dangerous = dangerous;
        this.role = role;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public int getDangerous() {
        return dangerous;
    }

    public void setDangerous(int dangerous) {
        this.dangerous = dangerous;
    }

}
