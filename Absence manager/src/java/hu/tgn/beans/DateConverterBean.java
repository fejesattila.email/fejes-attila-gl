package hu.tgn.beans;

import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import hu.tgn.services.DateConverterService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Remote;

@Stateful
@LocalBean
@Remote(DateConverterService.class)
public class DateConverterBean implements DateConverterService {

    TreeSet<Integer> workOutDays = new TreeSet<>();
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    private void loadWorkOutDays() {
        File file = new File("./src/java/hu/tgn/data/2018OUTDAYS.txt");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                workOutDays.add((int) (formatter.parse(line).getTime() / 86400000));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(DateConverterBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    @Override
    public List<Date> workDays(Date startDate, Date endDate) {
        loadWorkOutDays();
        List<Date> days = new ArrayList<>();
        int firstDate = (int) (startDate.getTime() / 86400000);
        int lastDate = (int) (endDate.getTime() / 86400000);
        for (int i = firstDate; i < lastDate + 1; i++) {
            if (!workOutDays.contains(i)) {
                days.add(new Date(i * 86400000));
            }
        }
        return days;
    }
}
