package hu.tgn.beans;

import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.dtos.UserDTO;
import hu.tgn.entitys.UserEntity;
import hu.tgn.enums.Role;
import hu.tgn.mapper.UserMapper;
import hu.tgn.services.UserService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.swing.text.DateFormatter;

@Stateless
@LocalBean
public class UserBean implements UserService {
    
    @EJB
    private PositionBean positionBean;
    
    @PersistenceContext
    EntityManager em;
    
    @EJB
    private UserMapper mapper;
    
    @Override
    public void insertUser(EmployeesDTO emp) {
        DateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
        String birthday = dayFormat.format(emp.getBirthday());
        
        String userName = emp.getFirstname().substring(0, 2) + emp.getLastname().substring(0, 2) + birthday.substring(2, 4);
        String userPass = birthday.substring(2, 4) + birthday.substring(5, 7) + birthday.substring(8, 10);
        Role role = Role.valueOf(positionBean.positionByID(emp.getPositionID()).getRole());
        
        UserEntity user = new UserEntity(
                emp.getEmployeeId(),
                userName,
                userPass,
                "", role, 0, 1, 0, 0, 0);
        em.persist(user);
    }
    
    @Override
    public UserDTO getUserById(Integer id) {
        UserDTO user = null;
        try {
            user = mapper.Entity2DTO((UserEntity) em.createNamedQuery("User.findById").setParameter("id", id).getSingleResult());
        } catch (NoResultException ex) {
            System.out.println("ERROR: No user found with given id!");
            return user;
        }
        return user;
    }
    
    @Override
    public UserDTO getUserByUsername(String username) {
        UserDTO user = null;
        try {
            user = mapper.Entity2DTO((UserEntity) em.createNamedQuery("User.findByUsername").setParameter("username", username).getSingleResult());
        } catch (NoResultException ex) {
            System.out.println("ERROR: No user found wuth given username!");
            return user;
        }
        return user;
    }
    
    @Override
    public List<UserDTO> getAllUser() {
        ArrayList<UserEntity> users = (ArrayList) em.createNamedQuery("User.getAll").getResultList();
        ArrayList<UserDTO> usersDTO = new ArrayList<>();
        for (UserEntity user : users) {
            usersDTO.add(mapper.Entity2DTO(user));
        }
        return usersDTO;
    }
    
    @Override
    public void updateUser(UserDTO userDTO) {
        UserEntity user = mapper.DTO2Entity(userDTO);
        em.merge(user);
    }
    
    @Override
    public void deleteUser(Integer id) {
        //
    }
    
    public void persist(Object object) {
        em.persist(object);
    }
    
}
