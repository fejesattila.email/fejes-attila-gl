package hu.tgn.beans;

import hu.tgn.dtos.AbsenceRequestDTO;
import hu.tgn.dtos.AbsencesDTO;
import hu.tgn.entitys.AbsenceRequest;
import hu.tgn.entitys.Employees;
import hu.tgn.mapper.AbsenceRequestMapper;
import hu.tgn.services.AbsenceRequestService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
@Remote(AbsenceRequestService.class)
public class AbsenceRequestBean implements AbsenceRequestService {

    @EJB
    private EmployeeBean employeeBean;

    @EJB
    private EnumBean enumBean;

    @EJB
    private AbsenceRequestMapper absenceRequestMapper;

    @EJB
    private AbsenceBean absenceBean;

    @PersistenceContext
    private EntityManager em;

    static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //formatter.parse("2014-05-15")

    @Override
    public int insertAbsenceRequest(AbsenceRequestDTO ardto) {
        AbsenceRequest request = absenceRequestMapper.DTO2Entity(ardto);

        List<Date> workdays = workDays(ardto.getStartDate(), ardto.getEndDate());

        int vacationMaxNum = employeeBean.numberOfVacationThisYear(ardto.getEmployeeId());

        if (workdays.size() > vacationMaxNum) {
            System.out.println("NINCS ENNYI SZABID EXCEPTION");
        } else {
            em.persist(request);
            return request.getId();
        }
        return 0;
    }

    @Override
    public int modifyAbsenceRequest(AbsenceRequestDTO ardto) {
        ardto.setRequestType("MODIFY");
        AbsenceRequest request = em.find(AbsenceRequest.class, ardto.getId());
        em.merge(absenceRequestMapper.DTO2Entity(ardto));
        return request.getId();
    }

    @Override
    public int deleteAbsenceRequest(AbsenceRequestDTO ardto) {
        absenceBean.deleteAbsenceaByAbsenceReqest(ardto.getId());

        return em.createQuery("DELETE FROM AbsenceRequest a WHERE a.id =:id", AbsenceRequest.class)
                .setParameter("id", ardto.getId())
                .executeUpdate();
    }

    @Override
    public List<AbsenceRequestDTO> allPendingAbsenceRequest() {
        List<AbsenceRequest> ars = em.createQuery("SELECT a FROM AbsenceRequest a WHERE a.status = :status")
                .setParameter("status", "PENDING").getResultList();
        return absenceRequestMapper.entityList2DTOList(ars);
    }

    @Override
    public List<AbsenceRequestDTO> allAbsenceRequestByEmployee(int employeeID) {
        List<AbsenceRequest> ars = em.createQuery("SELECT a FROM AbsenceRequest a WHERE a.employeeId = :employee")
                .setParameter("employee", new Employees(employeeID)).getResultList();
        return absenceRequestMapper.entityList2DTOList(ars);
    }

    @Override
    public List<AbsenceRequestDTO> allAbsenceRequestByEmployeeAndAbsenceType(int employeeID, String absenceType) {
        List<AbsenceRequest> ars = em.createQuery("SELECT a FROM AbsenceRequest a WHERE "
                + "a.employeeId = :employee AND "
                + "a.type = :type")
                .setParameter("employee", new Employees(employeeID))
                .setParameter("type", absenceType)
                .getResultList();
        return absenceRequestMapper.entityList2DTOList(ars);
    }

    @Override
    public List<AbsenceRequestDTO> allAbsenceRequestByEmployeeAndAbsenceStatus(int employeeID, String absenceStatus) {
        List<AbsenceRequest> ars = em.createQuery("SELECT a FROM AbsenceRequest a WHERE "
                + "a.employeeId = :employee AND "
                + "a.status = :status")
                .setParameter("employee", new Employees(employeeID))
                .setParameter("status", absenceStatus)
                .getResultList();
        return absenceRequestMapper.entityList2DTOList(ars);
    }

    @Override
    public int absenceRequestApproved(AbsenceRequestDTO ardto) {

        AbsenceRequest request = em.find(AbsenceRequest.class, ardto.getId());

        if (!ardto.getRequestType().equals("NEW")) {
            absenceBean.deleteAbsenceaByAbsenceReqest(ardto.getId());
        }

        if (!ardto.getRequestType().equals("DELETE")) {
            List<Date> workdays = workDays(request.getStartDate(), request.getEndDate());
            int result = em.createQuery("UPDATE AbsenceRequest a SET a.status=:approved WHERE a.id =:id", AbsenceRequest.class)
                    .setParameter("approved", "APPROVED")
                    .setParameter("id", request.getId())
                    .executeUpdate();
            for (Date workday : workdays) {
                absenceBean.insertAbsence(new AbsencesDTO(
                        workday,
                        request.getStatus(),
                        request.getType(),
                        request.getId(),
                        request.getEmployeeId().getEmployeeId()));
            }
            return result;
        } else {
            return deleteAbsenceRequest(ardto);
        }
    }

    @Override
    public int absenceRequestRejected(AbsenceRequestDTO ardto) {
        return em.createQuery("UPDATE AbsenceRequest a SET a.status=:rejected WHERE a.id =:id", AbsenceRequest.class)
                .setParameter("rejected", "REJECTED")
                .setParameter("id", ardto.getId())
                .executeUpdate();
    }

    private List<Date> workDays(Date startDate, Date endDate) {
        TreeSet<Integer> workOutDays = new TreeSet<>();
        for (Date date : enumBean.getAllDate()) {
            workOutDays.add((int) (date.getTime() / 86400000));
        }
        List<Date> days = new ArrayList<>();
        int firstDate = (int) (startDate.getTime() / 86400000);
        int lastDate = (int) (endDate.getTime() / 86400000);
        for (int i = firstDate; i < lastDate + 1; i++) {
            if (!workOutDays.contains(i)) {
                days.add(new Date((long) (i * 86400000L) + 79200001L));
            }
        }
        return days;
    }

}
