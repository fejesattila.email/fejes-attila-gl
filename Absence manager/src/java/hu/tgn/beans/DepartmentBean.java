package hu.tgn.beans;

import hu.tgn.dtos.DepartmentsDTO;
import hu.tgn.entitys.Departments;
import hu.tgn.entitys.Employees;
import hu.tgn.mapper.DepartmentMapper;
import hu.tgn.services.DepartmentService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
@Remote(DepartmentService.class)
public class DepartmentBean implements DepartmentService {

    @EJB
    private DepartmentMapper depMapper;

    @PersistenceContext
    private EntityManager em;

    @Override
    public int insertDepartment(DepartmentsDTO ddto) {
        Departments departments = depMapper.DTO2Entity(ddto);
        em.persist(departments);
        return departments.getDepartmentId();
    }

    @Override
    public int modifyDepartment(DepartmentsDTO ddto) {
        return em.createQuery("UPDATE Departments d SET d.departmentName = '" + ddto.getDepartmentName()
                + "', d.departmentBossid =:emp"
                + " WHERE d.departmentId=:id", Departments.class)
                .setParameter("id", ddto.getDepartmentId())
                .setParameter("emp", new Employees(ddto.getDepartmentBossId())).executeUpdate();
    }

    @Override
    public void deleteDepartment(DepartmentsDTO ddto) {
        em.createQuery("DELETE FROM Departments d WHERE d.departmentId =:id", Departments.class)
                .setParameter("id", ddto.getDepartmentId())
                .executeUpdate();
    }

    @Override
    public List<DepartmentsDTO> allDepartments() {
        List<Departments> allDepartments = em.createNamedQuery("Departments.findAll").getResultList();
        List<DepartmentsDTO> ddtos = new ArrayList<>();
        for (Departments d : allDepartments) {
            ddtos.add(depMapper.Entity2DTO(d));
        }
        return ddtos;
    }

}
