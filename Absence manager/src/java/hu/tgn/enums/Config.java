package hu.tgn.enums;

public interface Config {

    int BASENUMBEROFVACATION = 20;
    int CHILDUNDER17 = 17;
    int ONECHILD = 2;
    int TWOCHILDREN = 4;
    int MORETHANTWOCHILD = 7;
    int HANDICAPPEDCHILD = 2;
    int EMPLOYEEHANDICAPPED = 5;
    int UNDER18 = 5;
    int DANGEROUSCIRCUMSTANCE = 5;
    int[][] EXTRADAYSBYAGE
            = {
                {24, 20},
                {25, 21},
                {26, 21},
                {27, 21},
                {28, 22},
                {29, 22},
                {30, 22},
                {31, 23},
                {32, 23},
                {33, 24},
                {34, 24},
                {35, 25},
                {36, 25},
                {37, 26},
                {38, 26},
                {39, 27},
                {40, 27},
                {41, 28},
                {42, 28},
                {43, 29},
                {44, 29},
                {45, 30}};
}
