package hu.tgn.enums;

public enum Role {
    ADMIN,
    MANAGER,
    RECEPTIONIST,
    EMPLOYEE
}
