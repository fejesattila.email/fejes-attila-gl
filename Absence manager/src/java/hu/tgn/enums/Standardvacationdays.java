package hu.tgn.enums;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "STANDARDVACATIONDAYS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Standardvacationdays.findAll", query = "SELECT s FROM Standardvacationdays s")
    , @NamedQuery(name = "Standardvacationdays.findByAge", query = "SELECT s FROM Standardvacationdays s WHERE s.age = :age")
    , @NamedQuery(name = "Standardvacationdays.findByDays", query = "SELECT s FROM Standardvacationdays s WHERE s.days = :days")})
public class Standardvacationdays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "AGE")
    private int age;
    @Basic(optional = false)
    @Column(name = "DAYS")
    private int days;

    public Standardvacationdays() {
    }

    public Standardvacationdays(int age) {
        this.age = age;
    }

    public Standardvacationdays(int age, int days) {
        this.age = age;
        this.days = days;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.age;
        hash = 29 * hash + this.days;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Standardvacationdays other = (Standardvacationdays) obj;
        if (this.age != other.age) {
            return false;
        }
        if (this.days != other.days) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Standardvacationdays{" + "age=" + age + ", days=" + days + '}';
    }

}
