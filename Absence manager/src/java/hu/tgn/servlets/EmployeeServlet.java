package hu.tgn.servlets;

import hu.tgn.beans.EmployeeBean;
import hu.tgn.beans.PositionBean;
import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.dtos.PositionsDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EmployeeServlet extends HttpServlet {

    @EJB
    private EmployeeBean employeeBean;

    @EJB
    private PositionBean positionBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<PositionsDTO> dlist = positionBean.allPositions();
        List<String> listWithNames = new ArrayList<>();
        for (PositionsDTO position : dlist) {
            listWithNames.add(String.format("%03d", position.getPositionId()) + " " + position.getPositionName());
        }
        request.setAttribute("list", listWithNames);
        request.getRequestDispatcher("WEB-INF/employeejsp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date birthDay = null;
        Date startDay = null;
        Date endDay = null;

        try {
            birthDay = formatter.parse(request.getParameter("birthday"));
            startDay = formatter.parse(request.getParameter("startofwork"));
            endDay = formatter.parse(request.getParameter("endofwork"));
        } catch (ParseException ex) {
            Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        EmployeesDTO edto = new EmployeesDTO(
                request.getParameter("firstname"),
                request.getParameter("lastname"),
                birthDay,
                Integer.parseInt(request.getParameter("handicapped")),
                Integer.parseInt(request.getParameter("contractstyle")),
                startDay,
                endDay,
                request.getParameter("email"),
                Integer.parseInt(request.getParameter("positionID").substring(0, 3)));

        int empNum = employeeBean.insertEmployee(edto);

        System.out.println("INFO: ____________________  EMPLOYEE NUMBER: " + empNum);
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>EMPLOYEE registered: " + empNum + "</h1>");
            out.println(" <a href=\"/AbsenceEJB/EmployeeServlet\">Back</a> ");
        }

//        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }
}
