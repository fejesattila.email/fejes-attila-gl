package hu.tgn.servlets;

import hu.tgn.beans.EnumBean;
import hu.tgn.beans.PositionBean;
import hu.tgn.dtos.PositionsDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PositionServlet extends HttpServlet {

    @EJB
    private PositionBean positionBean;

    @EJB
    private EnumBean enumBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> listWithNames = enumBean.getAllRole();

        request.setAttribute("list", listWithNames);

        request.getRequestDispatcher("WEB-INF/positionjsp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PositionsDTO pdto = new PositionsDTO(request.getParameter("positionname"), Integer.parseInt(request.getParameter("dangerous")), request.getParameter("positionrolename"));

        int posNum = positionBean.insertPosition(pdto);
        System.out.println("INFO: ____________________  POSITION NUMBER: " + posNum);
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>Position registered: " + posNum + "</h1>");
            out.println(" <a href=\"/AbsenceEJB/PositionServlet\">Back</a> ");
        }
    }
}
