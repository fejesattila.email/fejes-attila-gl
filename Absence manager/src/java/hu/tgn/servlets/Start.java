package hu.tgn.servlets;

import hu.tgn.beans.AbsenceBean;
import hu.tgn.beans.EmployeeBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Start extends HttpServlet {

    @EJB
    private AbsenceBean absenceBean;

    @EJB
    private EmployeeBean employeeBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);

        if (null == session) {
            request.setAttribute("username", "Nincs");

        } else {
            LoginSessionBean loginSessionBean = (LoginSessionBean) session.getAttribute("loginSessionBean");
            int empID = loginSessionBean.getLoggedUser().getId();
            int maxVacation = employeeBean.numberOfVacationThisYear(empID);
            int outVacation = absenceBean.numberOfAbsenceDays(empID, "GENERAL");
            request.setAttribute("username", loginSessionBean.getLoggedUser().getUserName());
            request.setAttribute("numofyearlyvacation", maxVacation);
            request.setAttribute("takedoutvacationdays", outVacation);
            request.setAttribute("leftvacationdays", maxVacation - outVacation);
            request.setAttribute("sickdays", absenceBean.numberOfAbsenceDays(empID, "SICKLEAVE"));
            request.setAttribute("vacpercent", (int) (outVacation / maxVacation));

        }

        request.getRequestDispatcher("WEB-INF/role_receptionist.jsp").forward(request, response);
    }

}
