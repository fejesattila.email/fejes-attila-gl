package hu.tgn.servlets;

import hu.tgn.dtos.UserDTO;
import java.io.Serializable;

public class LoginSessionBean implements Serializable {
    
    private boolean loggedIn;
    private boolean loggedOut;
    private int loginAttempts;
    private String errorMessage;
    private UserDTO loggedUser;
    
    public LoginSessionBean(){
        this.loggedIn = false;
        this.loggedOut = true;
        this.loginAttempts = 0;
        this.errorMessage = "";
        this.loggedUser = null;
    }
    
    public void setErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }
    
    public String getErrorMessage(){
        return this.errorMessage;
    }
    
    public void clearErrorMessages(){
        this.errorMessage = "";
    }
    
    public boolean isLoggedIn(){
        return this.loggedIn;
    }
    
    public void setLoggedIn(boolean loggedIn){
        this.loggedIn = loggedIn;
    }

    public UserDTO getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }    

    public boolean isLoggedOut() {
        return loggedOut;
    }

    public void setLoggedOut(boolean loggedOut) {
        this.loggedOut = loggedOut;
    }

    public int getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(int loginAttempts) {
        this.loginAttempts = loginAttempts;
    }
    
    

}