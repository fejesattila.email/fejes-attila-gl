package hu.tgn.servlets;

import hu.tgn.beans.LoginServiceBean;
import hu.tgn.dtos.UserDTO;
import hu.tgn.exceptions.InvalidUsernameOrPasswordException;
import hu.tgn.exceptions.ReachedMaximumLoginAttemptsException;
import hu.tgn.exceptions.UserDoesNotExistException;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
    
    @EJB
    private LoginServiceBean loginService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        HttpSession session = req.getSession();
        LoginSessionBean loginSessionBean = (LoginSessionBean)session.getAttribute("loginSessionBean");
        if (null == loginSessionBean) {
            loginSessionBean = new LoginSessionBean();
            session.setAttribute("loginSessionBean", loginSessionBean);
        }
        super.service(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        LoginSessionBean loginSessionBean = (LoginSessionBean)session.getAttribute("loginSessionBean");
        
        if (loginSessionBean.isLoggedIn()){
            request.getRequestDispatcher("WEB-INF/start.jsp").forward(request, response);
        } else { 
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        LoginSessionBean loginSessionBean = (LoginSessionBean)session.getAttribute("loginSessionBean");
        
        if("changepassword".equals(request.getParameter("action"))){
            String newPassword = "";
            if (request.getParameter("password1").equals(request.getParameter("password2")) ){
                newPassword = request.getParameter("password1");
                if ( loginService.changePassword(loginSessionBean.getLoggedUser().getId(), newPassword) ){
                    loginSessionBean.setLoggedUser(loginService.getUserData(loginSessionBean.getLoggedUser().getId()));
                    loginSessionBean.clearErrorMessages();
                    loginSessionBean.setErrorMessage("A jelszó megváltozott!");
                }
            } else {
                loginSessionBean.clearErrorMessages();
                loginSessionBean.setErrorMessage("A két jelszó nem egyezik!");
                
            }
            
        }
        
        UserDTO user = null;
        
        try {
            user = loginService.login(request.getParameter("username"), request.getParameter("password"), session.getId());
            
            loginSessionBean.setLoggedIn(true);
            loginSessionBean.setLoggedUser(user);
            loginSessionBean.setLoginAttempts(0);
            loginSessionBean.clearErrorMessages();
            //request.getRequestDispatcher("WEB-INF/start.jsp").forward(request, response);
            response.sendRedirect(request.getContextPath() + "/Start");
        } catch (InvalidUsernameOrPasswordException ex) {
            loginSessionBean.setLoginAttempts(loginSessionBean.getLoginAttempts()+1);
            loginSessionBean.clearErrorMessages();
            loginSessionBean.setErrorMessage("Érvénytelen felhasználónév vagy jelszó! Próbálkozás: " + loginSessionBean.getLoginAttempts() + " / 3");
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        } catch (ReachedMaximumLoginAttemptsException ex){
            loginSessionBean.clearErrorMessages();
            loginSessionBean.setErrorMessage("A 3 sikertelen bejelentkezési kísérlet következtében fiókját zároltuk! A hozzáférés újbóli aktiválása érdekében lépjen kapcsolatba egy adminisztrátorral!");
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        } catch (UserDoesNotExistException | EJBException ex){
            loginSessionBean.clearErrorMessages();
            loginSessionBean.setErrorMessage("A felhasználó nem létezik!");
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        }
    }
}