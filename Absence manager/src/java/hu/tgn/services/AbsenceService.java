package hu.tgn.services;

import hu.tgn.dtos.AbsencesDTO;
import hu.tgn.dtos.EmployeesDTO;
import java.util.Date;
import java.util.List;

public interface AbsenceService {

    public int insertAbsence(AbsencesDTO adto);

    public int modifyAbsence(AbsencesDTO adto);

    void deleteAbsence(AbsencesDTO adto);

    public List<EmployeesDTO> allEmployeesOutOfOffice(Date today);

    public int numberOfAbsenceDays(int employee, String absenceType);

    List<AbsencesDTO> allAbsenceByAbsenceReqest(int absenceRequest);

    public void deleteAbsenceaByAbsenceReqest(int absenceRequest);

    List<AbsencesDTO> allAbsenceByEmployee(int emoloyeeID);

}
