package hu.tgn.services;

import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.dtos.UserDTO;
import java.util.List;

public interface UserService {

    public void insertUser(EmployeesDTO emp);

    public UserDTO getUserById(Integer id);

    public UserDTO getUserByUsername(String username);

    public List<UserDTO> getAllUser();

    public void updateUser(UserDTO userDTO);

    public void deleteUser(Integer id);

}
