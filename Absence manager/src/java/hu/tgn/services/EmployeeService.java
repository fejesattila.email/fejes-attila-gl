package hu.tgn.services;

import hu.tgn.dtos.EmployeesDTO;
import java.util.List;

public interface EmployeeService {

    public int insertEmployee(EmployeesDTO edto);

    public EmployeesDTO getEmployeeById(int id);

    public int modifyEmployee(EmployeesDTO edto);

    void delete(EmployeesDTO edto);

    public List<EmployeesDTO> getEmployeesByName(String name);

    public int numberOfVacationThisYear(int id);

    public List<EmployeesDTO> allEmployees();

    public List<EmployeesDTO> allEmployeesByBossID(int bossID);

}
