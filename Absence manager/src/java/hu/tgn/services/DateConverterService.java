package hu.tgn.services;

import java.util.Date;
import java.util.List;

public interface DateConverterService {

    public List<Date> workDays(Date startDate, Date endDate);

}
